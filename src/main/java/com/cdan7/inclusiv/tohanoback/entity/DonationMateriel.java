package com.cdan7.inclusiv.tohanoback.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
@Entity
@Data
@NoArgsConstructor
public class DonationMateriel extends Donation {

	@Column(name="DescriptionMateriel", nullable=true, length=255)
	private String descriptionMateriel;

	@Column(name="QuantiteMateriel", nullable=true, length=10)
	private int quantiteMateriel;

	@Column(name="DateLivraison", nullable=true)
	private LocalDate dateLivraison;

	@Column(name="lieuLivraison", nullable=true)
	private String lieuLivraison;

	@Column(name="idBesoinMateriel", nullable=true)
	private Long idBesoinMateriel;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DemandeAide demandeAide;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DonateurParticulier donateurParticulier;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DonateurAssociation donateurAssociation;
}