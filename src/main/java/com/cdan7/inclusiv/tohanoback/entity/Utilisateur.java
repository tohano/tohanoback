package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.boot.model.relational.Sequence;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.Serializable;


/**
 * 	Classe Utilisateur.
 * 	Classe mère des classes Malade, et donateur.
 * 	Contient tous les champs permettant d'identifier tout utilisateur ayant un compte dans Tohano.
 * 	L'utilisateur est relié à la classe retour. Retour devrait avoir l'id de l'utilisateur comme attribut
 * 	avec comme indication manyToOne dans Retour(Unidirectionnelle).
 */
@MappedSuperclass
@Data
@NoArgsConstructor
public class Utilisateur implements Serializable {
	/*
	@Transient
	@JsonIgnore
	private static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	*/
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idUtilisateur;

	@Column(length = 150)
	private String mail;

	@Column(length = 255)
	private String motDePasse;
/*	public void setPassword(String rawPassword) {
		this.motDePasse = passwordEncoder.encode(rawPassword);
	}
*/
	@Column(length = 120)
	private String pseudo;

	@Column(length = 13)
	private String contact;

	@Column(length = 60)
	private String adresse;
}