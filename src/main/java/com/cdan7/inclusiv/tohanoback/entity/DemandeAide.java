package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Table(name="demande_aide")
@Entity
@Transactional
public class DemandeAide implements Serializable {

	@Column(name = "idDemandeAide", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idDemandeAide;

	@Column(name = "titreDemandeAide", nullable = true, length = 150)
	private String titreDemandeAide;

	@Column(name = "descriptionDemandeAide", nullable = true, length = 255)
	private String descriptionDemandeAide;

	@Column(name = "dateDemandeAide", nullable = true, length = 50)
	private LocalDate dateDemandeAide;

	@Column(name = "dateLimite", nullable = true, length = 50)
	private LocalDate dateLimite;

	@Column(name = "lieu", nullable = false, length = 150)
	private String lieu;

	@Column(name = "long", nullable = true, length = 150)
	private float longitude;

	@Column(name = "lat", nullable = true, length = 150)
	private float latitude;

	@Column(name = "statut", nullable = true)
	private boolean statut;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private Malade malade;
	//@JoinColumn(name = "malade_id_utilisateur", referencedColumnName = "id_utilisateur", nullable = true)

	@OneToMany(
			mappedBy = "demandeAide",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	//@JoinColumn(name="")
	private List<Media> mediaLists;

	@OneToMany(
			mappedBy = "demandeAide",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<BesoinMateriel> besoinMaterielLists = new ArrayList<>();

	@OneToOne(
			mappedBy = "demandeAide",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private BesoinFinancier besoinFinancier;

	@OneToMany(
			mappedBy = "demandeAide",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	List<DonationFinancier> donationFinancierLists = new ArrayList<>();

	@OneToMany(
			mappedBy = "demandeAide",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	List<DonationMateriel> donationMaterielLists = new ArrayList<>();
}