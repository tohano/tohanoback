package com.cdan7.inclusiv.tohanoback.entity;

import java.time.LocalDate;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *
 */
@MappedSuperclass
@Data
@NoArgsConstructor
public class Donation {
	@Column(name="IdDonation", nullable=false)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idDonation;

	@Column(name = "dateDonation", nullable = false, length = 255)
	private LocalDate dateDonation;


}