package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class BesoinMateriel extends Besoin {
	@Column(length = 75)
	private String intituleBesoinMateriels;
	@Column
	private int quantiteBesoinMateriel;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DemandeAide demandeAide;

}