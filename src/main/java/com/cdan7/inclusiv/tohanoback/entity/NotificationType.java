package com.cdan7.inclusiv.tohanoback.entity;

public enum NotificationType {
    DemandeAideNonValideMalade,
    RapelRdvMaladeDonateur,
    DemandeValiderMalade,
    SignalementRecuAdmin,
    DonationVenantDonnateurPourMaladeAdmin,
    DonationRecuMalade,
    FinanciereCompletMaladeAdmin,
    RetourRecuAdmin
}
