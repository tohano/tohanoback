package com.cdan7.inclusiv.tohanoback.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Classe représentant un donateur. C'est une classe générique par laquelle hérite tout ce qui peut représenter
 * un donateur. Cette classe hérite aussi de la classe Utilisateur.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
public abstract class Donateur extends Utilisateur {

	@Column(length = 60)
	private String nomDonateur;

}