package com.cdan7.inclusiv.tohanoback.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui représente un particulier qui souhaite faire des dons. Elle hérite de la classe Donateur.
 */
@Entity
@Data
@NoArgsConstructor
public class DonateurParticulier extends Donateur {

	@Column(length = 60)
	private String prenomParticulier;

	@OneToMany(
			mappedBy = "donateurParticulier",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<DonationFinancier> donationFinancierLists = new ArrayList<>();

	@OneToMany(
			mappedBy = "donateurParticulier",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<DonationMateriel> donationMaterielLists = new ArrayList<>();

	@OneToMany(
			mappedBy = "donateurParticulier",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<Retour> retourLists = new ArrayList<>();
}