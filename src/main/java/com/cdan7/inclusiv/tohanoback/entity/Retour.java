package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.cdan7.inclusiv.tohanoback.entity.Utilisateur;
import org.springframework.transaction.event.TransactionalEventListener;

import java.io.Serializable;

@Entity
@Data
@Table(name = "retour")
@NoArgsConstructor
public class Retour implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idRetour;

	@Column(length = 255)
	private String description;

	@Column
	private int cible;

	@Column
	private int note;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private Malade malade;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DonateurParticulier donateurParticulier;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DonateurAssociation donateurAssociation;
}