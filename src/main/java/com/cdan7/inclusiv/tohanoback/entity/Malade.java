package com.cdan7.inclusiv.tohanoback.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un malade. Un malade possède une fiche médicale qu'il met à jour régulièrement.
 * Les fiches médicales s'ajoutent au fur et à mesure qu'il fait des mises à jour permettant ainsi le suivi
 * de son état médicale.
 *
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "malade")
public class Malade extends Utilisateur {

	@Column(length = 60)
	private String nomMalade;

	@Column(length = 120)
	private String prenomMalade;

	@Column
	private LocalDate dateNaissance;

	@Column(length = 12)
	private String cinMalade;

	@OneToMany(
			mappedBy = "malade",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<FicheMedicale> ficheMedicales = new ArrayList<>();

	@OneToMany(
			mappedBy = "malade",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<DemandeAide> demandeAides = new ArrayList<>();

	@OneToMany(
			mappedBy = "malade",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<Retour> retours = new ArrayList<>();


}