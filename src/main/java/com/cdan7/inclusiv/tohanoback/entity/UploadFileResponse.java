package com.cdan7.inclusiv.tohanoback.entity;

import lombok.Data;

@Data
public class UploadFileResponse {
    private boolean isError;
    private String fileName;
    private String fileLink;

    public UploadFileResponse(boolean isError, String fileName, String fileLink) {
        this.isError = isError;
        this.fileName = fileName;
        this.fileLink = fileLink;
    }

    public UploadFileResponse(boolean isError, String fileName) {
        this.isError = isError;
        this.fileName = fileName;
    }
}
