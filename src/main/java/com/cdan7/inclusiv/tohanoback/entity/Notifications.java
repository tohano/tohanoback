package com.cdan7.inclusiv.tohanoback.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class Notifications implements Serializable {

	@Column(name = "idNotification", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idNotification;

	@Column(name = "date", nullable = false, length = 50)
	private LocalDate date;

	@Column(name = "estLu", nullable = false, length = 50)
	private boolean estLu;

	@Column(name = "idSource", nullable = false, length = 50)
	private long idSource;

	@Column(name = "idDestination", nullable = false, length = 50)
	private long idDestination;

	@Column(name = "typeNotification", nullable = false)
	private NotificationType typeNotification;
}