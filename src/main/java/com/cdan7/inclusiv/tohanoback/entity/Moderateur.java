package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.Serializable;

@Entity
@Table(name="moderateur")
@Data
@NoArgsConstructor
public class Moderateur implements Serializable{

    @Transient
    @JsonIgnore
    private static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private static final long serialVersionUID = 1L;

    @Column(name="idModerateur", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idModerateur;

    @Column(name = "mailModerateur", nullable = false, length = 150)
    private String mail;

    @Column(name = "pseudoModerateur", nullable = false, length = 50)
    private String pseudoModerateur;

    @Column(name = "passwordModerateur", nullable = false ,length = 255)
    private String motDePasse;

    public void setPassword(String rawPassword) {
        this.motDePasse = passwordEncoder.encode(rawPassword);
    }

}
