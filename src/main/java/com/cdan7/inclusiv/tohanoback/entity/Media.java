package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="media")
public class Media implements Serializable {

	@Column(name="idMedia", nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idMedia;


	@Column(name="fichier", nullable = true)
	private String fichier;

	@Column(name="fileName", nullable = true)
	private String fileName;

	@Column(name = "typeMedia", nullable = true, length = 150)
	private String typeMedia;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DemandeAide demandeAide;
	//@JoinColumn(name = "demande_aide_id_demande_aide", referencedColumnName = "id_demande_aide")

}