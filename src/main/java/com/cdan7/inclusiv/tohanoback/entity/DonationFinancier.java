package com.cdan7.inclusiv.tohanoback.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@NoArgsConstructor
public class DonationFinancier extends Donation {
	@Column(name="MontantAideFinancier", nullable=true)
	private double montantAideFinancier;

	@Column(name="RaisonDonationFinancier", nullable=true, length=255)
	private String raisonDonationFinancier;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DemandeAide demandeAide;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DonateurParticulier donateurParticulier;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	private DonateurAssociation donateurAssociation;

	@Column
	private String idStripe;

	@Column
	private String etatPaiement;

}