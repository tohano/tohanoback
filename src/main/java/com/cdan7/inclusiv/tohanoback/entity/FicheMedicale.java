package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class FicheMedicale {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idFiche;

	@Column(length = 60)
	private String maladie;

	@Column
	private LocalDate dateDebut;

	@Column
	private LocalDate dateConsulation;

	@Column(length = 255)
	private String description;

	@Column
	private boolean estUrgent;

	@Column(length = 60)
	private String nomMedecin;

	@Column
	private String onmMedecin;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.MERGE)
	public Malade malade;
}