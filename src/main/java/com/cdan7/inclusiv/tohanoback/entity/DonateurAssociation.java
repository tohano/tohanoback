package com.cdan7.inclusiv.tohanoback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui représente une association. Cette classe est un type de donateur.
 * Elle hérite donc de la classe Donateur.
 */
@Entity
@Data
@NoArgsConstructor
public class DonateurAssociation extends Donateur {
	@Column(length = 150)
	private String objetAssociation;

	@Column
	private long nombreAdherentAssociation;

	@Column(length = 60)
	private String presidentAssociation;

	@OneToMany(
			mappedBy = "donateurAssociation",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<DonationFinancier> donationFinancierLists = new ArrayList<>();

	@OneToMany(
			mappedBy = "donateurAssociation",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<DonationMateriel> donationMaterielLists = new ArrayList<>();

	@OneToMany(
			mappedBy = "donateurAssociation",
			cascade = CascadeType.MERGE,
			orphanRemoval = true
	)
	private List<Retour> retourLists = new ArrayList<>();
}