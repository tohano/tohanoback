package com.cdan7.inclusiv.tohanoback.exception;

public class DonateurAssociationNotFound extends RuntimeException{
    public DonateurAssociationNotFound(Long id){
        super("Le malade avec l' id " + id + " est introuvable ");
    }
}
