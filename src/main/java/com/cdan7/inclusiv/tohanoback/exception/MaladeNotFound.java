package com.cdan7.inclusiv.tohanoback.exception;

public class MaladeNotFound extends RuntimeException{
    public MaladeNotFound(Long id){
        super("Le malade avec l' id " + id + " est introuvable ");
    }
}