package com.cdan7.inclusiv.tohanoback.exception;

public class MediaNotFound extends RuntimeException{
    public MediaNotFound(Long id){
        super("Le Media avec l' id " + id + " est introuvable ");
    }
}
