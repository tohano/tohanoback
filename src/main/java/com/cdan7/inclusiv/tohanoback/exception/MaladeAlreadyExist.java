package com.cdan7.inclusiv.tohanoback.exception;

public class MaladeAlreadyExist extends RuntimeException{
    public MaladeAlreadyExist(String email){
        super("Le malade avec l' email " + email + " existe ");
    }
}
