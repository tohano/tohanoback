package com.cdan7.inclusiv.tohanoback;

import jakarta.servlet.MultipartConfigElement;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@SpringBootApplication
public class TohanobackApplication {

	public static void main(String[] args) {
		SpringApplication.run(TohanobackApplication.class, args);
	}

	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		int maxFileSizeInMb = 50; // La taille maximale du fichier en mégaoctets (Mo)
		int maxRequestSizeInMb = 50; // La taille maximale de la requête en mégaoctets (Mo)

		// Convertir la taille maximale de fichier et de requête en bytes
		long maxFileSizeInBytes = maxFileSizeInMb * 1024 * 1024;
		long maxRequestSizeInBytes = maxRequestSizeInMb * 1024 * 1024;

		MultipartConfigElement configElement = new MultipartConfigElement(null, maxFileSizeInBytes, maxRequestSizeInBytes, 0);
		return configElement;
	}

}
