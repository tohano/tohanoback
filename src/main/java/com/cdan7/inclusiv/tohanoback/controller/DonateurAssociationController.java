package com.cdan7.inclusiv.tohanoback.controller;
import com.cdan7.inclusiv.tohanoback.entity.DonateurAssociation;
import com.cdan7.inclusiv.tohanoback.entity.DonateurAssociation;
import com.cdan7.inclusiv.tohanoback.service.DonateurAssociationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class DonateurAssociationController {
    @Autowired
    DonateurAssociationService donateurAssociationService;

    @PostMapping("/inscription/donateurassociation")
    ResponseEntity<Long> ajoutDonateurAssociation(@RequestBody DonateurAssociation newDonateurAssociation){
        donateurAssociationService.ajouter(newDonateurAssociation);
        return ResponseEntity.ok(newDonateurAssociation.getIdUtilisateur());
    }

    @GetMapping("/listdonateurassociation")
    List<DonateurAssociation> listDonateurAssociation(){
        List<DonateurAssociation> liste = donateurAssociationService.getAll();
        return liste;
    }


}
