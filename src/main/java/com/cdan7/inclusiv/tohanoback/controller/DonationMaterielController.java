package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.DonateurAssociation;
import com.cdan7.inclusiv.tohanoback.entity.DonationFinancier;
import com.cdan7.inclusiv.tohanoback.entity.DonationMateriel;
import com.cdan7.inclusiv.tohanoback.service.DemandeAideService;
import com.cdan7.inclusiv.tohanoback.service.DonateurAssociationService;
import com.cdan7.inclusiv.tohanoback.service.DonationMaterielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:3000")
public class DonationMaterielController {

    @Autowired
    DonationMaterielService donationMaterielService;

    @Autowired
    DemandeAideService demandeAideService;

    @Autowired
    DonateurAssociationService donateurAssociationService;
    @PostMapping("/donationmat/ajout/{idDemandeAide}/{idDonateurAssociation}")
    public void ajoutDonationMateriel(@PathVariable("idDemandeAide") Long idDemandeAide
            , @PathVariable("idDonateurAssociation") Long idDonateurAssociation
            , @RequestBody DonationMateriel donationMateriel){

        DemandeAide demandeAide = demandeAideService.getById(idDemandeAide);
        DonateurAssociation donateurAssociation = donateurAssociationService.getById(idDonateurAssociation);
        donationMateriel.setDemandeAide(demandeAide);

        donationMateriel.setDonateurAssociation(donateurAssociation);
        donationMateriel.setDateDonation(LocalDate.now());
        System.out.println(donationMateriel.getDescriptionMateriel());
        donationMaterielService.ajouter(donationMateriel);

    }
    @GetMapping("/gettotaldonationbyidbesoinmateriel/{id}")
    public int getTotalDonationByIdBesoinMateriel(@PathVariable("id") Long id){
        return donationMaterielService.getTotalDonationByIdBesoinMateriel(id);
    }

    @GetMapping("getDonationMaterielByDonateur/{idDemandeAide}/{idDonateur}")
    List<DonationMateriel> getDonationMaterielByDonateur(@PathVariable Long idDemandeAide,@PathVariable Long idDonateur){
       return donationMaterielService.getDonationMaterielByDonateur(idDemandeAide,idDonateur);
    }
}
