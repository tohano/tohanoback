package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.Moderateur;
import com.cdan7.inclusiv.tohanoback.entity.Utilisateur;
import com.cdan7.inclusiv.tohanoback.service.ConnexionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
public class ConnexionController {
    @Autowired
    ConnexionService connexionService;

    @PostMapping("/connexion")
    public String connexionDonateurParticulier(@RequestBody Utilisateur utilisateur){
        return connexionService.connecterUtilisateur(utilisateur);
    }

    @PostMapping("/admin/connexion")
    public String connexionModerateur(@RequestBody Moderateur moderateur){
        return connexionService.connecterModerateur(moderateur);
    }


}
