package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.Malade;
import com.cdan7.inclusiv.tohanoback.service.DemandeAideService;
import com.cdan7.inclusiv.tohanoback.service.MaladeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:3000")
public class DemandeAideController {
    @Autowired
    DemandeAideService demandeAideService;
    @Autowired
    MaladeService maladeService;

    @PostMapping("/patient/addInfoRequest/{idmalade}")
    ResponseEntity<Long> ajoutDemandeAide(@PathVariable("idmalade")Long idMalade, @RequestBody DemandeAide demandeAide){
        Malade malade = new Malade();
        malade.setIdUtilisateur(idMalade);
        demandeAide.setMalade(malade);
        //demandeAide.setMalade(maladeService.getById(demandeAide.);
        System.out.println(demandeAide.getMalade().getIdUtilisateur());
        demandeAide.setDateDemandeAide(LocalDate.now());
        DemandeAide demandeAideAjouter = demandeAideService.ajouter(demandeAide);

        if(demandeAideAjouter!=null){
            return ResponseEntity.ok(demandeAideAjouter.getIdDemandeAide());
        }else {
            return null;
        }
    }

    @GetMapping("/demandeaide/list")
    public List<DemandeAide> demandeAideList(){

        return demandeAideService.getAll();
    }

    @GetMapping("/demandeaide/{idDemandeAide}")
    public Optional<DemandeAide> demandeAide(@PathVariable("idDemandeAide") Long idDemandeAide){

        return demandeAideService.findById(idDemandeAide);
    }

    @GetMapping("/test")
    public List<Object[]> getDemandesAideDetails() {
        return demandeAideService.getDemandesAideDetails();
    }

    /*
        public List<DemandeAide> test(){
        return demandeAideService.details();
    }*/
    @PutMapping("/demandeaide/updateStatut/{idDemandeAide}")
    public ResponseEntity<String> updateJobOfferStatusPoste(@PathVariable Long idDemandeAide) {
        try {
            DemandeAide demandeAide = demandeAideService.getById(idDemandeAide);

            if (demandeAide != null) {
                demandeAide.setStatut(true);
                demandeAideService.save(demandeAide);

                return ResponseEntity.ok("Le statut de la demande a été mis à jour avec succès.");
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("demande non trouvée");
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Erreur lors de la mise à jour du statut de la demande", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getvalidatedemandeaide")
    public List<DemandeAide> getValidateDemandeAide(){
        return demandeAideService.getValidateDemande();
    }
}
