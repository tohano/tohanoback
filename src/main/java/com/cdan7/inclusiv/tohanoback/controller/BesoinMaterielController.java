package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.BesoinMateriel;
import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.service.BesoinMaterielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class BesoinMaterielController {

    @Autowired
    private BesoinMaterielService besoinMaterielService;

    @GetMapping("/getBesoinsMaterielDuDemande/{idDemandeAide}")
    public List<BesoinMateriel> getBesoinsMaterielsByDemandeAide(@PathVariable Long idDemandeAide){
        DemandeAide demandeAide = new DemandeAide();
        demandeAide.setIdDemandeAide(idDemandeAide);
        return besoinMaterielService.getByDemandeAide(demandeAide);
    }
}
