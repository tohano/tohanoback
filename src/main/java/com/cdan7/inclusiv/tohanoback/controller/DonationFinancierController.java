package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.DonationFinancier;
import com.cdan7.inclusiv.tohanoback.service.DemandeAideService;
import com.cdan7.inclusiv.tohanoback.service.DonationFinanciereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class DonationFinancierController {
    @Autowired
    DonationFinanciereService donationFinanciereService;
    @Autowired
    DemandeAideService demandeAideService;
    @PostMapping("/donate/{idDemandeAide}/{idDonateur}")
    ResponseEntity<Long> faireDonation(@PathVariable Long idDemandeAide,@PathVariable Long idDonateur,@RequestBody DonationFinancier donationFinancier) {
        donationFinanciereService.faireUneDonationFianciere(idDemandeAide,idDonateur,donationFinancier);
        //System.out.println(donationFinancier +"  id :" + idDemandeAide);
        return ResponseEntity.ok(donationFinancier.getIdDonation());
    }

    @GetMapping("/ao")
    List<DonationFinancier> makaDonationTest(){
        return donationFinanciereService.getAll();
    }


    @PostMapping("donation/save")
    Long ajouterDonation(@RequestBody DonationFinancier donationFinancier){
        donationFinanciereService.ajouter(donationFinancier);
        return donationFinancier.getIdDonation();
    }

    @GetMapping("donation/getall/{idDemandeAide}")
    Float getDonsByDemandeAide(@PathVariable Long idDemandeAide){
        Float total = (float) donationFinanciereService.getMontantRecolte(idDemandeAide);
        return total;
    }
    
    @GetMapping("getDonationByDonateur/{idDemandeAide}/{idDonateur}")
    List<DonationFinancier> getDonationByDonateur(@PathVariable Long idDemandeAide,@PathVariable Long idDonateur){
       return donationFinanciereService.getDonationByDonateur(idDemandeAide,idDonateur);
    }
    
    
}
