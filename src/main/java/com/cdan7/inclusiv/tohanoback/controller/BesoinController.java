package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.BesoinFinancier;
import com.cdan7.inclusiv.tohanoback.entity.BesoinMateriel;
import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.service.BesoinService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("http://localhost:3000")
public class BesoinController {
    @Autowired
    BesoinService besoinService;


    public BesoinMateriel ajoutUn(BesoinMateriel besoinMateriel){
        return besoinService.ajoutBesoinMateriel(besoinMateriel);
    }
    @PostMapping("/addBesoin")
    public void addBesoin(@RequestBody String str){
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(str);
            JsonNode besoinMaterielsNode = node.get("besoinMateriels");
            //List<BesoinMateriel> besoinMateriels = mapper.convertValue(node.get("besoinMateriels"), List.class);
            List<BesoinMateriel> besoinMateriels = mapper.readValue(besoinMaterielsNode.toString(), new TypeReference<List<BesoinMateriel>>(){});
            BesoinFinancier besoinFinancier = mapper.convertValue(node.get("besoinFinancier"), BesoinFinancier.class);
            DemandeAide demandeAide = mapper.convertValue(node.get("demandeAide"),DemandeAide.class);
            System.out.println("ito demande aide" + demandeAide.getIdDemandeAide());
            besoinFinancier.setDemandeAide(demandeAide);

            System.out.println(demandeAide.toString());
            if(besoinFinancier.getMontantDemandee() > 0 ){
                besoinService.ajoutBesoinFinancier(besoinFinancier);
            }
            if(besoinMateriels!=null){
                for(BesoinMateriel materiel : besoinMateriels){
                    materiel.setDemandeAide(demandeAide);
                }
                besoinMateriels.stream().map(this::ajoutUn).collect(Collectors.toList());
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        System.out.println(str);
    }
}
