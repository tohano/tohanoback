package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.DonateurParticulier;
import com.cdan7.inclusiv.tohanoback.entity.Malade;
import com.cdan7.inclusiv.tohanoback.service.DonateurParticulierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
public class DonateurParticulierController {
    @Autowired
    DonateurParticulierService donateurParticulierService;

    @PostMapping("/inscription/donateurparticulier")
    DonateurParticulier ajoutDonateurParticulier(@RequestBody DonateurParticulier donateurParticulier){
        return donateurParticulierService.ajouter(donateurParticulier);
    }


}
