package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.Malade;
import com.cdan7.inclusiv.tohanoback.service.MaladeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class MaladeController {

    @Autowired
    MaladeService maladeService;

    @PostMapping("/inscritpion/malade")
    ResponseEntity<Long> ajoutMalade(@RequestBody Malade newMalade){
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println(newMalade.getMail() + " " + newMalade.getMotDePasse());
        maladeService.ajouter(newMalade);
        return ResponseEntity.ok(newMalade.getIdUtilisateur());
    }

    @GetMapping("/listpatient")
    List<Malade> listMalade(){
        List<Malade> liste = maladeService.getAll();
        return liste;
    }

}
