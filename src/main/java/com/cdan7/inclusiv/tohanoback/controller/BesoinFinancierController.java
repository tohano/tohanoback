package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.BesoinFinancier;
import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.service.BesoinFinancierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class BesoinFinancierController {

    @Autowired
    BesoinFinancierService besoinFinancierService;

    @GetMapping("/getBesoinFinancierByDemande/{idDemandeAide}")
    public List<BesoinFinancier> getByDemandeAide(@PathVariable("idDemandeAide") Long idDemandeAide){
        DemandeAide demandeAide = new DemandeAide();
        demandeAide.setIdDemandeAide(idDemandeAide);
        return besoinFinancierService.getByDemandeAide(demandeAide);
    }
}
