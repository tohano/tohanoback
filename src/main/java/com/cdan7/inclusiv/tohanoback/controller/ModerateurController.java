package com.cdan7.inclusiv.tohanoback.controller;

import com.cdan7.inclusiv.tohanoback.entity.Moderateur;
import com.cdan7.inclusiv.tohanoback.service.ModerateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
public class ModerateurController {
    @Autowired
    ModerateurService moderateurService;

    @PostMapping("/inscription/moderateur")
    public Moderateur ajouterModerateur(@RequestBody Moderateur moderateur){
        return moderateurService.ajouter(moderateur);
    }

}
