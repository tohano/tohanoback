package com.cdan7.inclusiv.tohanoback.controller;


import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.Media;
import com.cdan7.inclusiv.tohanoback.entity.UploadFileResponse;
import com.cdan7.inclusiv.tohanoback.repository.MediaRepository;
import com.cdan7.inclusiv.tohanoback.service.UploadFileService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.List;


@Slf4j
@NoArgsConstructor
@RestController
@CrossOrigin(origins ="http://localhost:3000")
public class MediaController {

    @Autowired
    private  MediaRepository mediaRepository;

    @Autowired
    private UploadFileService uploadFileService;

    @Value("${project.image}")
    private String path;
    //@PostMapping("/files/uploadfile")

    public UploadFileResponse uploadFile( MultipartFile file, Long idDemandeAide){
        try {
            DemandeAide demandeAide = new DemandeAide();
            demandeAide.setIdDemandeAide(idDemandeAide);
            Media uploadFile = uploadFileService.saveUploadFile(file, demandeAide, path);
            return new UploadFileResponse(true, uploadFile.getFileName(),creteUploadFileLink(uploadFile.getFileName()));

        } catch (Exception e) {
            log.error("Upload File Failed.", e);
            return new UploadFileResponse(true,file.getOriginalFilename());
        }
    }

    private String creteUploadFileLink(String fileName) {
        return ServletUriComponentsBuilder.fromCurrentRequest().replacePath("/files/" + fileName).toUriString();
    }
/*
    @GetMapping("/files/{fileName}")
    public ResponseEntity<Resource> getFile(@PathVariable String fileName) {

        var fileUpload = uploadFileService.getFileUpload(fileName);
        var grpData = new ByteArrayResource(fileUpload.getFichier());

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, fileUpload.getTypeMedia())

                 * Alternative:
                 * .cntentType(MediaType
                 * .valueOf(fileUpload.
                 * getFileType()))

                .cacheControl(CacheControl.maxAge(Duration.ofSeconds(60)).cachePrivate().mustRevalidate())
                .body(grpData);
    }*/

    @GetMapping("/download/{id}")
    public ResponseEntity<?> fileDownload(@PathVariable("id") Long id) throws IOException {
        Media media = mediaRepository.findById(id).orElseThrow();
        byte[] imageData = this.uploadFileService.getFileUpload(id,media.getFichier());

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/jpeg"))
                .body(imageData);
    }
    @PostMapping("/files/multi_uploadfile")
    public List<UploadFileResponse> uploadMultiFiles(@RequestParam("file") List<MultipartFile> files, @RequestParam("demandeAide") Long idDemandeAide) {

        //return files.stream().map(this::uploadFile).collect(Collectors.toList());

        for(MultipartFile f : files){
            this.uploadFile(f, idDemandeAide);
        }
        return null;
    }

    @GetMapping("/files/multi_downloadfile/{id}")
    public List<Media> downloadMultiFiles(@PathVariable("id") Long id){
        DemandeAide demandeAide = new DemandeAide();
        demandeAide.setIdDemandeAide(id);
        return mediaRepository.findByDemandeAide(demandeAide);
    }

    @GetMapping("/getmedialistbyiddemandeaide/{id}")
    public Object[] getIdMediaListByIdDemandeAide(@PathVariable("id") Long id){
        return mediaRepository.getIdMediaByDemandeAide(id);
    }
}
