package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DonateurAssociation;
import com.cdan7.inclusiv.tohanoback.exception.DonateurAssociationAlreadyExist;
import com.cdan7.inclusiv.tohanoback.exception.DonateurAssociationNotFound;
import com.cdan7.inclusiv.tohanoback.repository.DonateurAssociationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DonateurAssociationService implements IService<DonateurAssociation> {

    @Autowired
    DonateurAssociationRepository donateurAssociationRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public DonateurAssociation ajouter(DonateurAssociation donateurAssociation) {
        if(!donateurAssociationRepository.existsByMail(donateurAssociation.getMail())) {
            String hashedMotDePasse = passwordEncoder.encode(donateurAssociation.getMotDePasse());
            donateurAssociation.setMotDePasse(hashedMotDePasse);
            donateurAssociationRepository.save(donateurAssociation);
        }
        return null;
    }

    @Override
    public List<DonateurAssociation> getAll() {
        return donateurAssociationRepository.findAll();
    }

    @Override
    public DonateurAssociation getById(Long id) {
        return donateurAssociationRepository.findById(id).orElseThrow( ()-> new DonateurAssociationNotFound(id));
    }

    @Override
    public void supprimer(Long id) {
        donateurAssociationRepository.deleteById(id);
    }

    @Override
    public void mettreAJour(DonateurAssociation donateurAssociation) {
        donateurAssociationRepository.save(donateurAssociation);
    }

}
