package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DonateurParticulier;
import com.cdan7.inclusiv.tohanoback.repository.DonateurParticulierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DonateurParticulierService implements IService<DonateurParticulier> {

    @Autowired
    DonateurParticulierRepository donateurParticulierRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public DonateurParticulier ajouter(DonateurParticulier donateurParticulier) {
        if(!donateurParticulierRepository.existsByMail(donateurParticulier.getMail())) {
            //Crypter le mot de passe
            String hashedPassword = passwordEncoder.encode(donateurParticulier.getMotDePasse());
            donateurParticulier.setMotDePasse(hashedPassword);
            //enregistrer le mot de passe
            donateurParticulierRepository.save(donateurParticulier);
        }
        return null;
    }

    @Override
    public List<DonateurParticulier> getAll() {
        return donateurParticulierRepository.findAll();
    }

    @Override
    public DonateurParticulier getById(Long id) {
        return donateurParticulierRepository.findById(id).get();
    }

    @Override
    public void supprimer(Long e) {
        donateurParticulierRepository.delete(this.getById(e));
    }

    @Override
    public void mettreAJour(DonateurParticulier donateurParticulier) {
        donateurParticulierRepository.save(donateurParticulier);
    }

}
