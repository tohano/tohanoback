package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.BesoinFinancier;
import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.repository.BesoinFinancierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class BesoinFinancierService implements IService<BesoinFinancierService> {

    @Autowired
    BesoinFinancierRepository besoinFinancierRepository;

    @Override
    public BesoinFinancierService ajouter(BesoinFinancierService besoinFinancierService) {
        return null;
    }

    @Override
    public List<BesoinFinancierService> getAll() {
        return null;
    }

    @Override
    public BesoinFinancierService getById(Long id) {
        return null;
    }

    @Override
    public void supprimer(Long e) {

    }

    @Override
    public void mettreAJour(BesoinFinancierService besoinFinancierService) {

    }

    public List<BesoinFinancier> getByDemandeAide(DemandeAide demandeAide){
        return besoinFinancierRepository.getByDemandeAide(demandeAide);
    }
}
