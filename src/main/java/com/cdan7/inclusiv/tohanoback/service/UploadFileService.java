package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.Media;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface UploadFileService {
    Media saveUploadFile(MultipartFile file, DemandeAide demandeAide, String path) throws Exception;

    byte[] getFileUpload(Long id, String path) throws IOException;
}
