package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.Malade;
import com.cdan7.inclusiv.tohanoback.exception.MaladeAlreadyExist;
import com.cdan7.inclusiv.tohanoback.exception.MaladeNotFound;
import com.cdan7.inclusiv.tohanoback.repository.MaladeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaladeService implements IService<Malade>{
    @Autowired
    MaladeRepository maladeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Malade ajouter(Malade malade) {
        if(!maladeRepository.existsByMail(malade.getMail())){
            String hashedPassword = passwordEncoder.encode(malade.getMotDePasse());
            malade.setMotDePasse(hashedPassword);
            maladeRepository.save(malade);
        }
        else {
            throw new MaladeAlreadyExist(malade.getMail());
        }
        return null;
    }


    @Override
    public List<Malade> getAll() {
        return maladeRepository.findAll();
    }

    @Override
    public Malade getById(Long id) {
        return maladeRepository.findById(id).orElseThrow( ()-> new MaladeNotFound(id));
    }

    @Override
    public void supprimer(Long id) {
        maladeRepository.deleteById(id);
    }

    @Override
    public void mettreAJour(Malade malade) {

    }

    public String connecterMalade(Malade malade) {
        String mail = malade.getMail();
        String password = malade.getMotDePasse();
        if(maladeRepository.existsByMail(mail)){
            if(passwordEncoder.matches(password,maladeRepository.findByMail(mail).getMotDePasse())){
                return "{\"id\": "+maladeRepository.findByMail(mail).getIdUtilisateur()+", \"userType\": \"malade\"}";
            }

        }
        return "{\"id\": 0, \"userType\": \"internaute\"}";
    }
}
