package com.cdan7.inclusiv.tohanoback.service.impl;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.Media;
import com.cdan7.inclusiv.tohanoback.repository.MediaRepository;
import com.cdan7.inclusiv.tohanoback.service.UploadFileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;


@Service
@RequiredArgsConstructor
@Slf4j
public class UploadFileServiceImpl implements UploadFileService {

    @Autowired
    MediaRepository mediaRepository;
    @Override
    public Media saveUploadFile(MultipartFile file, DemandeAide demandeAide, String path) throws Exception {
        Media media = new Media();
        String randomID = UUID.randomUUID().toString();
        String fileName = randomID.concat(file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")));
        String filePath = path + File.separator+fileName;
        File f = new File(path);

        if (!f.exists()){
            f.mkdir();
        }
        Files.copy(file.getInputStream(), Paths.get(filePath));

        if (mediaRepository.existsByfileName(file.getOriginalFilename())) {
            log.info("This file {} has been already existed: ", file.getOriginalFilename());
            return media;
        }
        media.setFileName(file.getOriginalFilename()); media.setTypeMedia(file.getContentType());
        media.setFichier(path + fileName); media.setDemandeAide(demandeAide);

        mediaRepository.save(media);
        return media;
    }

    @Override
    public byte[] getFileUpload(Long id, String path) throws IOException {
        byte[] image = Files.readAllBytes(new File(path).toPath());
        return image;
    }
}
