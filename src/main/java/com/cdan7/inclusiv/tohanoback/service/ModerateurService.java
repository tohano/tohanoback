package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.Moderateur;
import com.cdan7.inclusiv.tohanoback.repository.ModerateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ModerateurService implements IService<Moderateur> {

    @Autowired
    ModerateurRepository moderateurRepository;

    @Override
    public Moderateur ajouter(Moderateur moderateur) {
        if(!moderateurRepository.existsByMail(moderateur.getMail())) {
            String rawPassword = moderateur.getMotDePasse();
            moderateur.setPassword(rawPassword);
            moderateurRepository.save(moderateur);
        }
        return null;
    }

    @Override
    public List<Moderateur> getAll() {
        return moderateurRepository.findAll();
    }

    @Override
    public Moderateur getById(Long id) {
        return moderateurRepository.findById(id).get();
    }

    @Override
    public void supprimer(Long e) {
        moderateurRepository.delete(this.getById(e));
    }

    @Override
    public void mettreAJour(Moderateur moderateur) {
        moderateurRepository.save(moderateur);
    }

}
