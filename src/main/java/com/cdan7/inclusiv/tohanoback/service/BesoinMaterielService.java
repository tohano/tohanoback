package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.BesoinMateriel;
import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.repository.BesoinMaterielRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class BesoinMaterielService implements IService<BesoinMateriel>{

    @Autowired
    BesoinMaterielRepository besoinMaterielRepository;

    @Override
    public BesoinMateriel ajouter(BesoinMateriel besoinMateriel) {
        return null;
    }

    @Override
    public List<BesoinMateriel> getAll() {
        return null;
    }

    @Override
    public BesoinMateriel getById(Long id) {
        return null;
    }

    @Override
    public void supprimer(Long e) {

    }

    @Override
    public void mettreAJour(BesoinMateriel besoinMateriel) {

    }

    public List<BesoinMateriel> getByDemandeAide(DemandeAide demandeAide){
        return besoinMaterielRepository.getByDemandeAide(demandeAide);
    }
}
