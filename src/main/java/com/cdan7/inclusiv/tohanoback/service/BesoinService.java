package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.BesoinFinancier;
import com.cdan7.inclusiv.tohanoback.entity.BesoinMateriel;
import com.cdan7.inclusiv.tohanoback.repository.BesoinFinancierRepository;
import com.cdan7.inclusiv.tohanoback.repository.BesoinMaterielRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BesoinService {

    @Autowired
    BesoinFinancierRepository besoinFinancierRepository;

    @Autowired
    BesoinMaterielRepository besoinMaterielRepository;

    public BesoinMateriel ajoutBesoinMateriel(BesoinMateriel besoinMateriel){
        return besoinMaterielRepository.save(besoinMateriel);
    }

    public BesoinFinancier ajoutBesoinFinancier(BesoinFinancier besoinFinancier){
        return besoinFinancierRepository.save(besoinFinancier);
    }
}
