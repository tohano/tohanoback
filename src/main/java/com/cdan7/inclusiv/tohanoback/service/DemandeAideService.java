package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.repository.DemandeAideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DemandeAideService implements IService<DemandeAide> {

    @Autowired
    DemandeAideRepository demandeAideRepository;

    @Override
    public DemandeAide ajouter(DemandeAide demandeAide) {
        return demandeAideRepository.save(demandeAide);
    }

    @Override
    public List<DemandeAide> getAll() {
        return demandeAideRepository.findAll();
    }

    public List<DemandeAide> getValidateDemande(){
        return demandeAideRepository.findBySatutTrue();
    }
    
    @Override
    public DemandeAide getById(Long id) {
        return demandeAideRepository.getReferenceById(id);
    }

    @Override
    public void supprimer(Long e) {
        demandeAideRepository.deleteById(e);
    }

    @Override
    public void mettreAJour(DemandeAide demandeAide) {
        demandeAideRepository.save(demandeAide);
    }

    public String save(DemandeAide demandeAide) {
        demandeAideRepository.save(demandeAide);
        return "demande enregistrée avec succès.";
    }

    public Optional<DemandeAide> findById(Long id){
        return demandeAideRepository.findById(id);
    }



    public  List<Object[]> getDemandesAideDetails(){
        return demandeAideRepository.listeDemandeAideFotsiny();
    }
}
