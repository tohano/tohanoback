package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.Malade;
import com.cdan7.inclusiv.tohanoback.entity.Moderateur;
import com.cdan7.inclusiv.tohanoback.entity.Utilisateur;
import com.cdan7.inclusiv.tohanoback.repository.DonateurAssociationRepository;
import com.cdan7.inclusiv.tohanoback.repository.DonateurParticulierRepository;
import com.cdan7.inclusiv.tohanoback.repository.MaladeRepository;
import com.cdan7.inclusiv.tohanoback.repository.ModerateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ConnexionService {

    @Autowired
    MaladeRepository maladeRepository;

    @Autowired
    ModerateurRepository moderateurRepository;

    @Autowired
    DonateurParticulierRepository donateurParticulierRepository;

    @Autowired
    DonateurAssociationRepository donateurAssociationRepository;

    public String connecterUtilisateur(Utilisateur utilisateur){
        //Les données du formulaire
        String mail = utilisateur.getMail();
        String password = utilisateur.getMotDePasse();

        //Le comparateur de mots de passes
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        //Les données de sessions
        Long userId = 0L;
        String userType = "internaute";

        if(maladeRepository.existsByMail(mail)) {
            String mdp = maladeRepository.findByMail(mail).getMotDePasse();
            System.out.println("MDP : "+mdp);

            if (passwordEncoder.matches(password, mdp)) {
                System.out.println("OK tonga ato ny mail an'i malade sady marin n mdp");
                userId =  maladeRepository.findByMail(mail).getIdUtilisateur();
                userType = "malade";
                return "{\"id\": " + userId + ", \"userType\": \"" + userType + "\"}";
            }
        }

        if(donateurParticulierRepository.existsByMail(mail)) {
            if (passwordEncoder.matches(password, donateurParticulierRepository.findByMail(mail).getMotDePasse())) {
                userId = donateurParticulierRepository.findByMail(mail).getIdUtilisateur();
                userType = "donateur";
                return "{\"id\": " + userId + ", \"userType\": \"" + userType + "\"}";
            }
        }

        if(donateurAssociationRepository.existsByMail(mail)) {
            if (passwordEncoder.matches(password, donateurAssociationRepository.findByMail(mail).getMotDePasse())) {
                userId = donateurAssociationRepository.findByMail(mail).getIdUtilisateur();
                userType = "donateur";
                return "{\"id\": " + userId + ", \"userType\": \"" + userType + "\"}";
            }
        }

        return "{\"id\": " + userId + ", \"userType\": \"" + userType + "\"}";
    }

    public String connecterModerateur(Moderateur moderateur) {
        //Les données du formulaire
        String mail = moderateur.getMail();
        String password = moderateur.getMotDePasse();

        //Le comparateur de mots de passes
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        //Les données de sessions
        Long userId = 0L;
        String userType = "internaute";

        if(moderateurRepository.existsByMail(mail)){
            if(passwordEncoder.matches(password, moderateurRepository.findByMail(mail).getMotDePasse())){
                userId = moderateurRepository.findByMail(mail).getIdModerateur();
                userType = "moderateur";
                return "{\"id\": " + userId + ", \"userType\": \"" + userType + "\"}";
            }
        }

        return "{\"id\": " + userId + ", \"userType\": \"" + userType + "\"}";
    }
}
