package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DonationMateriel;
import com.cdan7.inclusiv.tohanoback.repository.DonationMaterielRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonationMaterielService implements IService<DonationMateriel>{

    @Autowired
    DonationMaterielRepository donationMaterielRepository;

    @Override
    public DonationMateriel ajouter(DonationMateriel donationMateriel) {
        return donationMaterielRepository.save(donationMateriel);
    }

    @Override
    public List<DonationMateriel> getAll() {
        return null;
    }

    @Override
    public DonationMateriel getById(Long id) {
        return null;
    }

    @Override
    public void supprimer(Long e) {

    }

    @Override
    public void mettreAJour(DonationMateriel donationMateriel) {

    }

    public int getTotalDonationByIdBesoinMateriel(Long id) {
        return donationMaterielRepository.getTotalDonationByIdBesoinMateriel(id);
    }

    public List<DonationMateriel> getDonationMaterielByDonateur(Long idDemandeAide,Long idDonateur){
        
        return donationMaterielRepository.getDonationMaterielByDonateur(idDemandeAide,idDonateur);
    }   
}
