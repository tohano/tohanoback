package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.Media;
import com.cdan7.inclusiv.tohanoback.exception.MediaNotFound;
import com.cdan7.inclusiv.tohanoback.repository.MediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MediaService implements IService<Media>{

    @Autowired
    MediaRepository mediaRepository;

    @Override
    public Media ajouter(Media media) {
        mediaRepository.save(media);

        return null;
    }

    @Override
    public List<Media> getAll() {

        return mediaRepository.findAll();
    }

    @Override
    public Media getById(Long id) {
        return mediaRepository.findById(id).orElseThrow(()-> new MediaNotFound(id));
    }

    @Override
    public void supprimer(Long e) {

    }

    @Override
    public void mettreAJour(Media media) {

    }

    public List<Media> getByDemandeAide(DemandeAide demandeAide){
        return mediaRepository.findByDemandeAide(demandeAide);
    }

    public Object[] getIdMediaByIdDemande(Long id){
        return mediaRepository.getIdMediaByDemandeAide(id);
    }
}
