package com.cdan7.inclusiv.tohanoback.service;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.DonateurAssociation;
import com.cdan7.inclusiv.tohanoback.entity.DonationFinancier;
import com.cdan7.inclusiv.tohanoback.repository.DemandeAideRepository;
import com.cdan7.inclusiv.tohanoback.repository.DonationFinanciereRepository;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.time.LocalDate;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DonationFinanciereService implements IService<DonationFinancier>{

    @Autowired
    DonationFinanciereRepository donationFinanciereRepository;

    @Autowired
    DemandeAideRepository demandeAideRepository;
    @Autowired
    DonateurAssociationService donateurAssociationService;

    @Override
    public DonationFinancier ajouter(DonationFinancier donationFinancier) {
        return null;
    }

    @Override
    public List<DonationFinancier> getAll() {
        return null;
    }

    @Override
    public DonationFinancier getById(Long id) {
        return null;
    }

    @Override
    public void supprimer(Long e) {

    }

    @Override
    public void mettreAJour(DonationFinancier donationFinancier) {

    }


    public void faireUneDonationFianciere(Long idDemandeAide,Long idDonateur,DonationFinancier donationFinancier){
        Stripe.apiKey="sk_test_51OXPCnCUN88RfCuSZO5uuOXm94Als2NqOSw0XBU9WtxSGFNkrO1tyvj1mQHobazQdMoeRpegokUB9q7N2Hm1wGKm004Zw1ML9W";
        Map<String,Object> params= new HashMap<>();
        BigDecimal montant = BigDecimal.valueOf(new BigDecimal(donationFinancier.getMontantAideFinancier()).intValue());
        params.put("amount", montant);
        params.put("currency","MGA");
        params.put("description", donationFinancier.getRaisonDonationFinancier());
        params.put("source","tok_visa");
        DemandeAide demande = demandeAideRepository.getReferenceById(idDemandeAide);
        donationFinancier.setDemandeAide(demande);
        DonateurAssociation donateur = donateurAssociationService.getById(idDonateur);
        donationFinancier.setDonateurAssociation(donateur);
        try {
            Charge charge= Charge.create(params);
            donationFinancier.setEtatPaiement(charge.getStatus());
            donationFinancier.setIdStripe(charge.getId());
            donationFinancier.setDateDonation(LocalDate.now());
            donationFinanciereRepository.save(donationFinancier);
        }
        catch (StripeException e) {
            throw new RuntimeException(e);
        }

    }

    public double getMontantRecolte(Long idDemandeAide){
        double montantRecolte = 0;
        List<Float> dons = new ArrayList<>();
        dons = donationFinanciereRepository.getDonationFinanciereByDemandeAide(idDemandeAide);
        for (Float vola: dons) {
            montantRecolte+=vola;
        }
        return montantRecolte;
    }

    
    public List<DonationFinancier> getDonationByDonateur(Long idDemandeAide,Long idDonateur){
        
        return donationFinanciereRepository.getDonationByDonateur(idDemandeAide,idDonateur);
        
    }

}
