package com.cdan7.inclusiv.tohanoback.service;

import java.util.List;

public interface IService<E> {
    public E ajouter(E e);

    public List<E> getAll();

    public E getById(Long id);

    public void supprimer(Long e);

    public void mettreAJour(E e);

}
