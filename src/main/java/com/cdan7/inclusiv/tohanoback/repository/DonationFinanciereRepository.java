package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.DonationFinancier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DonationFinanciereRepository extends JpaRepository<DonationFinancier,Long> {
    @Query(value = "SELECT montant_aide_financier FROM donation_financier WHERE demande_aide_id_demande_aide = :idDemandeAide", nativeQuery = true)
    public List<Float> getDonationFinanciereByDemandeAide(@Param("idDemandeAide") Long idDemandeAide);

    @Query(value = "select * from donation_financier \r\n" + //
            "where demande_aide_id_demande_aide = :idDemandeAide \r\n" + //
            "and (donateur_association_id_utilisateur = :idDonateur or donateur_particulier_id_utilisateur = :idDonateur)", nativeQuery = true)
    public List<DonationFinancier> getDonationByDonateur(@Param("idDemandeAide") Long idDemandeAide,@Param("idDonateur") Long idDonateur);

   
}
