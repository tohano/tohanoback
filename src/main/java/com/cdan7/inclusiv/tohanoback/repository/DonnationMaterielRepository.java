package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DonationMateriel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonnationMaterielRepository extends JpaRepository<DonationMateriel, Long> {
}
