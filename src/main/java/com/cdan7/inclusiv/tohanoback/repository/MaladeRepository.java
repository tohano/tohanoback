package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.Malade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaladeRepository extends JpaRepository<Malade,Long> {
    boolean existsByMail(String email);

    boolean existsByMailAndMotDePasse(String email, String MotDePasse);
    Malade findByMail(String email);
}
