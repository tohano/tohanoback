package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.BesoinFinancier;
import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BesoinFinancierRepository extends JpaRepository<BesoinFinancier, Long> {
    List<BesoinFinancier> getByDemandeAide(DemandeAide demandeAide);
}
