package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DonateurAssociation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonateurAssociationRepository extends JpaRepository<DonateurAssociation, Long> {

    boolean existsByMail(String mail);

    DonateurAssociation findByMail(String mail);
}
