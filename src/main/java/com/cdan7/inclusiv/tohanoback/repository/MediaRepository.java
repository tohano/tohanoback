package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import com.cdan7.inclusiv.tohanoback.entity.Media;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface MediaRepository extends JpaRepository<Media, Long> {

    /*
     * Generated SQL: SELECT tbl_uploadfile.id FROM schema_uploadfile.tbl_uploadfile
     * WHERE tbl_uploadfile.file_name = ? LIMIT 1
     */
    boolean existsByfileName(String fileName);

    /*
     * Generate SQL: SELECT...FROM schema_uploadfile.tbl_uploadfile WHERE tbl_uploadfile.file_name = ?
     */
    Optional<Media> findByFileName(String fileName);

    //Optional<Media>findByDemandeAide(DemandeAide demandeAide);
    List<Media> findByDemandeAide(DemandeAide demandeAide);

    @Query(value="SELECT id_media FROM media \n" +
            "WHERE demande_aide_id_demande_aide=:id", nativeQuery = true)
    Object[] getIdMediaByDemandeAide(@Param("id") Long id);
}
