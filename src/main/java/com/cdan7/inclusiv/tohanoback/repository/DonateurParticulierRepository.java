package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DonateurParticulier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonateurParticulierRepository extends JpaRepository<DonateurParticulier, Long> {

    boolean existsByMail(String mail);
    boolean existsByMailAndMotDePasse(String mail, String motDePasse);

    DonateurParticulier findByMail(String mail);
}
