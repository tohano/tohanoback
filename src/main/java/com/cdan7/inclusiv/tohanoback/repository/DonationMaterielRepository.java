package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DonationFinancier;
import com.cdan7.inclusiv.tohanoback.entity.DonationMateriel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DonationMaterielRepository extends JpaRepository<DonationMateriel,Long> {
     @Query(value = "select * from donation_materiel \r\n" + //
            "where demande_aide_id_demande_aide = :idDemandeAide \r\n" + //
            "and (donateur_association_id_utilisateur = :idDonateur or donateur_particulier_id_utilisateur = :idDonateur)", nativeQuery = true)
    public List<DonationMateriel> getDonationMaterielByDonateur(@Param("idDemandeAide") Long idDemandeAide,@Param("idDonateur") Long idDonateur);
    
    @Query(value="SELECT sum(quantite_materiel) \n" +
            "FROM donation_materiel \n" +
            "WHERE id_besoin_materiel =:id", nativeQuery = true)
    public int getTotalDonationByIdBesoinMateriel(@Param("id") Long id);
}
