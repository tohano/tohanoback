package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.Malade;
import com.cdan7.inclusiv.tohanoback.entity.Moderateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModerateurRepository extends JpaRepository<Moderateur,Long> {
    boolean existsByMail(String email);
    Moderateur findByMail(String email);
}
