package com.cdan7.inclusiv.tohanoback.repository;

import com.cdan7.inclusiv.tohanoback.entity.DemandeAide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandeAideRepository extends JpaRepository<DemandeAide,Long> {
//    @Query("SELECT mal, da.id_demande_aide, da.description_demande_aide, da.lieu, da.statut, da.titre_demande_aide," +
//            "med.id_media, med.chemin_media, med.fichier, med.file_name, med.type_media FROM Malade mal ")
    /*@Query("SELECT mal, da.id_demande_aide, da.description_demande_aide, da.lieu, da.statut, da.titre_demande_aide, med.id_media, med.chemin_media, med.fichier, med.file_name, med.type_media \"\n" +
            "FROM Malade mal " +
            "JOIN mal.demandesAide da " +
            "JOIN da.media med " +
            "WHERE mal.idUtilisateur = :idUtilisateur")
    public List<DemandeAide> details();*/
    /*@Query("SELECT mal, da.id_demande_aide, da.description_demande_aide, da.lieu, da.statut, da.titre_demande_aide, med.id_media, med.chemin_media, med.fichier, med.file_name, med.type_media "
            + "FROM Malade mal "
            + "JOIN mal.demandesAide da "
            + "JOIN da.media med "
            + "WHERE mal.idUtilisateur = :idUtilisateur")
    public List<Object[]> details(); */

    /*@Query("SELECT mal, da.idDemandeAide, da.descriptionDemandeAide, da.lieu, da.statut, da.titreDemandeAide, med.idMedia, med.cheminMedia, med.fichier, med.fileName, med.typeMedia "
            + "FROM Malade mal "
            + "JOIN mal.demandesAide da "
            + "JOIN da.mediaList med "
            + "WHERE mal.idUtilisateur = :idUtilisateur")
    List<Object[]> getDemandesAideDetails(@Param("idUtilisateur") Long idUtilisateur);*/
    /*
    @Query("SELECT mal, da.idDemandeAide, da.descriptionDemandeAide, da.lieu, da.statut, da.titreDemandeAide, med.idMedia, med.cheminMedia, med.fichier, med.fileName, med.typeMedia "
            + "FROM Malade mal "
            + "JOIN mal.demandeAideList da "
            + "JOIN da.mediaList med "
            + "WHERE mal.idUtilisateur = :idUtilisateur")
    List<Object[]> demandesAideDetails(@Param("idUtilisateur") Long idUtilisateur);
*/

    @Query(value = "SELECT mal.*,  " +
            " da.id_demande_aide, da.description_demande_aide, da.lieu, da.statut, da.titre_demande_aide, " +
            " med.id_media, med.fichier, med.file_name, med.type_media " +
            "FROM malade AS mal " +
            "JOIN demande_aide AS da " +
            "ON mal.id_utilisateur = da.malade_id_utilisateur " +
            "JOIN media AS med " +
            "ON med.demande_aide_id_demande_aide = da.id_demande_aide ", nativeQuery = true)
    public List<Object[]> listeDemandeAideFotsiny();

    @Query(value="SELECT * " +
            "FROM demande_aide " +
            "WHERE statut = true", nativeQuery = true)
    List<DemandeAide> findBySatutTrue();
}
